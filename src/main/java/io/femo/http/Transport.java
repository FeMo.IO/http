package io.femo.http;

/**
 * Created by felix on 9/10/15.
 */
public enum Transport {

    HTTP, HTTPS
}
